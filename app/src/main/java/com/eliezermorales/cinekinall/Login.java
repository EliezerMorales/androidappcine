package com.eliezermorales.cinekinall;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

//import java.util.Random;


public class Login extends ActionBarActivity {


    private Button btnLogin;
    private TextView btnRegistrar;
    private EditText txtUsuarios,txtPassw;
    private Toolbar mToolbar;
    private CheckBox ChbRememberme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtUsuarios = (EditText) findViewById(R.id.txtUsuario);
        txtPassw = (EditText) findViewById(R.id.txtPass);
        btnRegistrar = (TextView) findViewById(R.id.btnRegistrar);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        ChbRememberme = (CheckBox) findViewById(R.id.ChbRememberme);

        btnRegistrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view){
                try {
                    Intent myIntent = new Intent(view.getContext(), Registre.class);

                    startActivityForResult(myIntent, 0);

                }catch (NullPointerException e){

                }

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtUsuarios.getText().length() == 0) {
                    txtUsuarios.requestFocus();

                    Bundle extras;
                    extras = getIntent().getExtras();
                    String uno = extras.getString("Usuario",null);
                    extras.getString("Usuario","null");
                    Toast.makeText(getApplicationContext(), "C", Toast.LENGTH_LONG).show();
                    return;

                }
                if (txtPassw.getText().length() == 0) {
                    txtPassw.requestFocus();
                    return;
                }

                Intent mainActivityIntent = new Intent(Login.this, Welcome.class);
                startActivity(mainActivityIntent);
                Login.this.finish();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}