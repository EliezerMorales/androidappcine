package com.eliezermorales.cinekinall;




import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.eliezermorales.cinekinall.titular;


public class HomeFragment extends Fragment {

    private ListView lvItems;
    private ListView lvLista;
    private ListView lvFavoritos;


    private titular[] datosTitular = new titular[]{
            new titular("Avengers 2", "Fecha Estreno: 2015", "Pelicula de accion en donde hay super heroes que quieren vengar la tierra pero esta es la parte dos donde un experimento de tony stark sale mal y de ahi sale toda la casaca."),
            new titular("Dragon Ball", "Fecha Estreno: 2015", "A alguien se le ocurre revivir a frezzer y fue el mejor deseo porque ahora saldra esta pelicula, la mas esperada del todo el año"),
            new titular("Home", "Fecha Estreno: 2015", "´Pelicual de Disney en donde un grupo de extraterrestres inofencivos con mayor capacidad intelectual vienen a invadir la tierra pero traen aca una guerra que probocara la destruccion de la tierra."),
            new titular("Grandes Heroes", "Fecha Estreno: 2014", "Esta pelicula es de comedia en donde un joven cuyo hermano muere le deja a un robot para qeu lo cude pero cuando descubre la causa de la muerte de su hermano quiere vengarla y pues para que seguir."),
            new titular("Piratas del Caribe", "Fecha Estreno: 2017", "Pelicula de piratas en donde buscan algun tesoro con el perla negra dirigida por el capitan jacks parrow es muy esperada por todos asi que no hay que perdersela."),
            new titular("4 Fantasticos", "Fecha Estreno: 2016", "Es una pelicula animada de marvel en la que se elasa de alguna forma en la pelicula de los vengadores para la 3ra y 4ta entrega de dicha pelicula."),
            new titular("The Hobbit 3", "Fecha Estreno: 2014", "La pelicula es la tercera edicion de una tri"),
            new titular("Bajo la misma estrella", "Fecha Estreno: 2013", "La Venganza de los Sith es la tercera pel�cula de Star Wars. Siendo una precuela, parte de una trilog�a situada cronol�gicamente varias d�cadas antes de las pel�culas originales de Star Wars (estrenadas desde 1977 hasta 1983), completa la saga y enlaza las dos trilog�as."),
            new titular("Step UP", "Fecha Estreno: 2016", "Es la primera etapa de una trilog�a llamada \"Goal!. Esta pel�cula se hizo con la plena cooperaci�n de FIFA, por lo que aparecen equipos reales en la pel�cula. La segunda entrega, Goal! 2: Living the Dream..., fue estrenada en febrero de 2007. La tercera y �ltima cuota, Goal! 3: Taking on the World, cuenta como Santiago participa en la Copa Mundial de F�tbol de 2006."),
            new titular("Noe", "Fecha Estreno: 2014", "Es el t�tulo de la pentalog�a cinematogr�fica de Walt Disney Pictures integrada por Piratas del Caribe: La maldici�n de La Perla Negra (2003), Piratas del Caribe: El cofre del hombre muerto (2006), Piratas del Caribe: En el fin del mundo (2007), Piratas del Caribe: En mareas misteriosas (2011) y Piratas del Caribe: Los hombres muertos no cuentan cuentos (2017).")

    };

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        lvLista = (ListView) v.findViewById(R.id.lvLista);
        AdaptadorTitulares adaptadorTitulares = new AdaptadorTitulares(getActivity(), datosTitular);
        lvLista.setAdapter(adaptadorTitulares);


        lvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentTitular = new Intent(getActivity(), titular.class);
                titular titularSeleccionado = (titular) parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("Titulo", titularSeleccionado.getTitulo());
                extras.putString("Descripcion", titularSeleccionado.getDescripcion());
                intentTitular.putExtras(extras);
                startActivity(intentTitular);
            }
        });

        return v;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();


      /*  switch (item.getItemId()) {
            case R.id.CtxHello:
                Toast.makeText(this, "Saying hello from context menu", Toast.LENGTH_LONG).show();
                return true;*/

        switch (item.getItemId()) {
            case R.id.CtxListTitularFavorito:
                try {
                    // lvFavoritos = (ListView) findViewById(R.id.CtxListTitularFavorito);

                    for (int x = 0; x <= datosTitular.length; x++) {
                        if (x == info.position) {
                            MovieManager.AgregarPelicula(datosTitular[x].getTitulo());
                            ArrayAdapter<String> Favs = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                                    MovieManager.AListaPelicula());
                            Toast.makeText(getActivity(), "Pelicula agregada", Toast.LENGTH_LONG).show();
                        }
                    }

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Error al agregar pelicula", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    class AdaptadorTitulares extends ArrayAdapter<titular> {
        public AdaptadorTitulares(Context context, titular[] datos) {
            super(context, R.layout.listitem_titular, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.listitem_titular, null);

            TextView tvTitulo = (TextView) item.findViewById(R.id.tvTitulo);
            TextView tvSubtitulo = (TextView) item.findViewById(R.id.tvSubTitulo);

            String tituloTitular = datosTitular[position].getTitulo();
            String subtituloTitular = datosTitular[position].getSubtitulo();

            tvTitulo.setText(tituloTitular);
            tvSubtitulo.setText(subtituloTitular);

            return item;
        }
    }
}
